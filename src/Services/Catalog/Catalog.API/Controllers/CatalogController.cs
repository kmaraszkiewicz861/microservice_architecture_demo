﻿using AutoMapper;
using Catalog.API.Entities;
using Catalog.API.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Catalog.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private IProductRepository _productRepository;

        private ILogger<CatalogController> _logger;

        private IMapper _mapper;

        public CatalogController(IProductRepository productRepository, ILogger<CatalogController> logger, IMapper mapper)
        {
            _productRepository = productRepository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<ProductModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetProdcuts()
        {
            var products = await _productRepository.GetProducts();

            return Ok(_mapper.Map<IEnumerable<ProductModel>>(products));
        }

        [HttpGet("{id:length(24)}", Name = "GetProduct")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ProductModel), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<ProductModel>> GetProductById(string id)
        {
            var product = await _productRepository.GetProduct(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ProductModel>(product));
        }

        [HttpGet("[action]/{category}", Name = "GetProductByCategory")]
        [ProducesResponseType(typeof(IEnumerable<ProductModel>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<IEnumerable<ProductModel>>> GetProductByCategory(string category)
        {
            return Ok(_mapper.Map<IEnumerable<ProductModel>>(await _productRepository.GetProductByCategory(category)));
        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<ProductModel>), (int)HttpStatusCode.Created)]
        public async Task<ActionResult<ProductModel>> CreateProduct([FromBody] ProductModel product)
        {
            await _productRepository.CreateProduct(_mapper.Map<Product>(product));

            return CreatedAtRoute("GetProduct", new { id = product.Id }, product);
        }

        [HttpPut]
        [ProducesResponseType(typeof(ProductModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductModel product)
        {
            return Ok(await _productRepository.UpdateProduct(_mapper.Map<Product>(product)));
        }

        [HttpDelete("{id:length(24)}", Name = "DeleteProduct")]
        [ProducesResponseType(typeof(ProductModel), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteProductById(string id)
        {
            return Ok(await _productRepository.DeleteProduct(id));
        }
    }
}
