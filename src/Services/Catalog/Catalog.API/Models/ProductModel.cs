﻿namespace Catalog.API.Entities
{
    public record ProductModel(string Id, string Name, string Category, 
        string Summary, string Description, string ImageFile, decimal Price)
    {

    }
}