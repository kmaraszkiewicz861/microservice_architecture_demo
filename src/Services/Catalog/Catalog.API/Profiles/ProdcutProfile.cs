﻿using AutoMapper;
using Catalog.API.Entities;

namespace Catalog.API.Profiles
{
    public class ProdcutProfile : Profile
    {
        public ProdcutProfile()
        {
            CreateMap<Product, ProductModel>();
        }
    }
}